package runner

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"

	"text/template"

	gitlabv1beta2 "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/api/v1beta2"
	gitlabutils "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers/utils"
	corev1 "k8s.io/api/core/v1"
)

// Config struct holds the values used to
// configure Runner Global options
type Config struct {
	Concurrent    int32
	CheckInterval int32
}

func userOptions(cr *gitlabv1beta2.Runner) Config {
	options := Config{Concurrent: 10, CheckInterval: 30}

	if cr.Spec.Concurrent != nil {
		options.Concurrent = *cr.Spec.Concurrent
	}

	if cr.Spec.CheckInterval != nil {
		options.CheckInterval = *cr.Spec.CheckInterval
	}

	return options
}

// ConfigMap returns the runner configmap object
func ConfigMap(cr *gitlabv1beta2.Runner) *corev1.ConfigMap {
	labels := gitlabutils.Label(cr, "runner", gitlabutils.RunnerType)

	templatesPath, err := getTemplatesPath()
	if err != nil {
		panic(err)
	}

	var gitlabURL string
	var configToml bytes.Buffer
	configTemplate := template.Must(template.ParseFiles(filepath.Join(templatesPath, "config.toml")))
	configTemplate.Execute(&configToml, userOptions(cr))

	entrypointScript := gitlabutils.ReadConfig(filepath.Join(templatesPath, "entrypoint.sh"))
	configureScript := gitlabutils.ReadConfig(filepath.Join(templatesPath, "configure.sh"))
	registrationScript := gitlabutils.ReadConfig(filepath.Join(templatesPath, "registration.sh"))
	aliveScript := gitlabutils.ReadConfig(filepath.Join(templatesPath, "check-live.sh"))

	// Gitlab URL should be used for GitLab instances
	if cr.Spec.GitLab != "" {
		gitlabURL = cr.Spec.GitLab
	}

	runnerConfigMap := gitlabutils.GenericConfigMap(labels["app.kubernetes.io/instance"]+"-config", cr.Namespace, labels)
	runnerConfigMap.Data = map[string]string{
		"ci_server_url":   gitlabURL,
		"config.toml":     configToml.String(),
		"entrypoint":      entrypointScript,
		"check-live":      aliveScript,
		"register-runner": registrationScript,
		"configure":       configureScript,
	}

	// update configmap with checksum in annotation
	gitlabutils.ConfigMapWithHash(runnerConfigMap)

	return runnerConfigMap
}

// the hasConfigTemplate function checks if the the user provided a custom
// config.toml file spec.config
// https://docs.gitlab.com/runner/register/#runners-configuration-template-file
func hasConfigTemplate(cr *gitlabv1beta2.Runner) bool {
	return cr.Spec.Configuration != ""
}

func getTemplatesPath() (string, error) {
	const rootTemplates = "/templates"
	if _, err := os.Stat(rootTemplates); err == nil {
		return rootTemplates, nil
	}

	wd, err := os.Getwd()
	if err != nil {
		return "", fmt.Errorf("getting working directory for templates %w", err)
	}

	projectTemplates := filepath.Join(wd, "hack/assets/templates")
	if _, err := os.Stat(projectTemplates); err == nil {
		return projectTemplates, nil
	}

	return "", fmt.Errorf("couldn't find templates directory %w", os.ErrNotExist)
}
